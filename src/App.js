import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Dashboard from './containers/DashBoard'
import Ejemplo1 from './containers/Ejemplo1'
import Ejemplo2 from './containers/Ejemplo2'
import Forms from './containers/Forms'
function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Dashboard}/>
                <Route path="/todo" component={Ejemplo1}/>
                <Route path="/timer" component={Ejemplo2}/>
                <Route path="/forms" component={Forms}/>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
