import React, {Component} from 'react'
import {Item, Image, Button, Icon,Label} from 'semantic-ui-react'

class Product extends Component {

    //Componente dedicado a la presentacion de un producto
    // consumme un array de objetos y los representa enn interfaz


    handleUpVote =()=> {  //usamos un arrow function para propagar el evento onClick y evitar el uso de bind
        this.props.onVote(this.props.id);
    }

    render() {

        return (
            //template jsx
            //aqui esta el evento onClick y es pasado por props a su componente padre
            <Item>
                <Image src={this.props.img}></Image>
                <Item.Content>
                    <Item.Header>{this.props.title}</Item.Header>
                    <Item.Meta>Description</Item.Meta>
                    <Item.Description>{this.props.description}</Item.Description>
                    <Button as='div' labelPosition='right'>
                        <Button onClick={this.handleUpVote} color='red'> 
                            <Icon name='heart'/>
                            Like
                        </Button>
                        <Label as='a' basic color='red' pointing='left'>
                            {this.props.votes}
                        </Label>
                    </Button>
                </Item.Content>
            </Item>

        );
    }
}

export default Product;