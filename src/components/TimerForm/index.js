import React, {Component} from 'react'
import {Card, Button, Form} from 'semantic-ui-react'
class TimerForm extends Component {

    state= {
        title:this.props.title || '',
        proyect:this.props.proyect || ''
    }

    handleTitleChange =(e)=>{
        this.setState({title:e.target.value})
    }
    handleProyectChange =(e)=>{
        this.setState({proyect:e.target.value})
    }

    handleSubmit =()=>{
        this.props.onFormSubmit({
            id:this.props.id,
            title:this.state.title,
            proyect:this.state.proyect
        });
    }


    render() {
        const submitText = this.props.title
            ? 'Update'
            : 'Create';
        return (

            <Form>
                <Card>
                    <Card.Content>
                        <Card.Header>Create/Update Timer</Card.Header>
                        <Form.Field>
                            <label>Title</label>
                            <input placeholder='title' onChange={this.handleTitleChange} defaultValue={this.state.title}/>
                        </Form.Field>
                        <Form.Field>
                            <label>Proyect</label>
                            <input placeholder='proyect' onChange={this.handleProyectChange} defaultValue={this.state.proyect}/>
                        </Form.Field>
                        </Card.Content>
                        <Card.Content extra>
                            <div className='ui two buttons'>
                                <Button onClick={this.handleSubmit} basic color='green'>
                                    {submitText}
                                </Button>
                                <Button onClick={this.props.onFormClose} basic color='red'>
                                    Decline
                                </Button>
                            </div>
                    </Card.Content>
                </Card>
            </Form>

        );
    }

}

export default TimerForm;