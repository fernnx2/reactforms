import React, {Component} from 'react'
import {Card,Button,Icon} from 'semantic-ui-react'

class Timer extends Component {

    handleTrashClick = () => {
        this.props.onTrashClick(this.props.id);
        };

    render() {
        const elapsedString = this.props.elapsed;
        return (
            <Card>
                <Card.Content>
                    <Card.Header>{this.props.title}</Card.Header>
                    <Card.Meta>{this.props.proyect}</Card.Meta>
                    <Card.Description>{elapsedString}</Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <div className="ui two buttons">
                        <Button onClick={this.handleTrashClick} icon>
                            <Icon name='trash'/>
                        </Button>
                        <Button onClick={this.props.onEditClick} icon>
                            <Icon name='edit'/>
                        </Button>
                    </div>
                    <Button primary>
                        Start
                        </Button>
                </Card.Content>
            </Card>
        );
    }
}

export default Timer;