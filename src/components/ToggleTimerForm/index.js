import React, {Component} from 'react'
import TimerForm from '../TimerForm'
import {Button, Icon} from 'semantic-ui-react'
class ToggleTimerForm extends Component {

    state ={
        isOpen: false
    }

    handleFormClose=()=>{
        this.setState({isOpen:false});
    }

    handleFormSubmit=(timer)=>{
        this.props.onFormSubmit={timer}
        this.setState({isOpen:false});
    }

    handleFormOpen =()=>{
        this.setState({isOpen:true});
    }

    render() {
        if (this.state.isOpen) {
            return (
                <TimerForm
                onFormSubmit={this.handleFormSubmit}
                onFormClose={this.handleFormClose}
                ></TimerForm>
            );
        } else {
            return (
                <Button onClick={this.handleFormOpen} icon>
                    <Icon name='plus'/>
                </Button>
            );

        }

    }
}

export default ToggleTimerForm;