import React, { Component } from 'react'
import {Button} from 'semantic-ui-react'
export default class ButtonWC extends Component {

    handleOnClick =(evt)=>{
        this.props.click(true);
    }

    render(){
        return(
           <Button onClick={this.handleOnClick}>{this.props.name}</Button>
        );
    }

}