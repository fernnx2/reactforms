import React, { Component } from 'react'
import {Item} from 'semantic-ui-react'
import Product from '../Product'

import {SeedProducts} from './seed'
class ProductList extends Component {

    //ProductList Componente dedicado a respresentar una lista de productos
    //Aqui se define el stado inicial para llenar todos los componentes Product
    //Cuando se propaga el evento desde product ProductList escucha y ejecuta la accion

    //se evita el contructor por que usamos arrow funtions
    state = {
        products:[]
    }

    componentDidMount(){
        this.setState({products: SeedProducts})
    }

    handleProductUpVote =(productId)=>{  //Se usa arrow function en los events para evitar el uso de bind
       
        //para actualizar el estado inmutable utilizamos map 
        //Este devuelve un nuevo objeto con las respectivas modificaciones
       let productsList = this.state.products.map((product)=>{
            if(product.id === productId){
                return Object.assign({},product,{votes:product.votes +1})
            }
            else{
                return product;
            }
        
        });
        //Luego cuando el estado es actualizado con setState React reacciona y actualiza la vista
        this.setState({products:productsList})
        
    }

    render(){
        //ordenamos e, stado por votos
        const productsSort = this.state.products.sort((a,b)=>(
            b.votes - a.votes
        ));

        //los recorremos con map y este devuelve una lista de componentes con data del productSort
        const productComponents = productsSort.map((product)=>
            <Product
            key={'product-'+ product.id}
            id={product.id}
            img={product.productImageUrl}
            title={product.title}
            description={product.description}
            votes={product.votes}
            onVote={this.handleProductUpVote}
            ></Product>
        );
        return(
            //ponemos todos los componentes deltro del jsx 
            <Item.Group>
                {productComponents}
            </Item.Group>
        );
    }

}

export default ProductList;