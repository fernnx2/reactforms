export const SeedProducts = [
    {
        id: 1,
        title: 'Yellow Pail',
        description: 'On-demand sand castle construction expertise.',
        url: '#',
        votes: 1,
        productImageUrl: '/assets/image/afghan-flower-2.gif'
    },
    {
        id: 2,
        title: 'Blue Pail',
        description: 'On-demand sand castle construction expertise xxx.',
        url: '#',
        votes: 2,
        productImageUrl: '/assets/image/albania-flower-2.gif'
    },
    {
        id: 3,
        title: 'Red Pail',
        description: 'On-demand sand castle construction expertise yyy.',
        url: '#',
        votes: 3,
        productImageUrl: '/assets/image/a-partridge-in-a-pear-tree-2.gif'
    }
]