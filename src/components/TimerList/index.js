import React, { Component } from 'react'
import EditTimer from '../EditTimer'
class TimerList extends Component {

    render(){

        const timers = this.props.timers.map((timer)=>(
            <EditTimer
                key={timer.id}
                id ={timer.id}
                title={timer.title}
                proyect = {timer.proyect}
                elapsed={timer.elapsed}
                runningSince={timer.runningSince}
                onFormSubmit={this.props.onFormSubmit}
                onTrashClick={this.props.onTrashClick}
                ></EditTimer>
        ));

        return(
            <div id="timers">
               {timers}
            </div>
        );
    }

}

export default TimerList;