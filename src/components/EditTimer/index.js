import React, {Component} from 'react'
import TimerForm from '../TimerForm'
import Timer from '../Timer'
class EditTimer extends Component {
    state = {
        editFormOpen: false
    }

    handleEditClick = () => {
        this.openForm();
    }
    handleFormClose = () => {
        this.closeForm();
    }

    handleSubmit = (timer) => {
        this
            .props
            .onFormSubmit(timer);
        this.closeForm();
    };
    closeForm = () => {
        this.setState({editFormOpen: false});
    };
    openForm = () => {
        this.setState({editFormOpen: true});
    };

    render() {
        if (this.state.editFormOpen) {
            return (
                <TimerForm
                    id={this.props.id}
                    title={this.props.title}
                    proyect={this.props.proyect}
                    onFormSubmit={this.handleSubmit}
                    onFormClose={this.handleFormClose}></TimerForm>
            );
        } else {
            return (
                <Timer
                    title={this.props.title}
                    id={this.props.id}
                    proyect={this.props.proyect}
                    elapsed={this.props.elapsed}
                    runningSince={this.props.runningSince}
                    onEditClick={this.handleEditClick}
                    onTrashClick={this.props.onTrashClick}
                    ></Timer>
            );
        }
    }
}

export default EditTimer;