import React, { Component } from 'react'
import {Container,Header} from 'semantic-ui-react'
import ProductList from './../../components/ProductList'
class Ejemplo1 extends Component{

    render(){

        return(
            <Container text>
                <Header as='h2'>List Products</Header>
                <ProductList></ProductList>
            </Container>
        );
    }

}

export default Ejemplo1;