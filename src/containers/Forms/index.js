import React, {Component} from 'react'
import {Card, Container,Form,Button,List, Message} from 'semantic-ui-react'
import ButtonWC from '../../components/Button'
export default class Forms extends Component {

    state ={
        names:[],
        fields:{
            apellido:'',
            email:''
        },
        camps:[],
        fieldsErrors:{}
    }

    //propagacion de evento para el componente Button
    exelentHandlerClick = (e) => {
        if (e) {
            alert('Exelente');
        }

    }
    grandiosoHandlerClick = (e) => {
        if (e) {
            alert('Grandioso');
        }
    }

    //submit
    handlerSubmitForm =(evt)=>{
        const name = this.refs.name.value; //obtengo el valor de input ref=name 
        const names = [...this.state.names,name]; //concateno el valor al array del state
        this.setState({names:names});
        this.refs.name.value='';
        evt.preventDefault();
    }

    //Controlando el input name 
    onNameChange =(evt)=>{
        this.setState({name:this.refs.name.value})
    }
    //input component controller
    handlerSubmitFormController =(evt)=>{
        
        const field = [...this.state.camps,this.state.fields]; //concateno el valor al array del state
        this.setState({camps:field});
        evt.preventDefault();
    }

    onInputChange=(evt)=>{
        const fields = this.state.fields;
        fields[evt.target.name]=evt.target.value;
        this.setState({fields});
    }

    render() {
        return (
            <Container>
                <Card>
                    <Card.Content>
                        <Card.Header>Buttons Events</Card.Header>
                    </Card.Content>
                    <Card.Content extra>
                        <div className='ui two buttons'>
                            <ButtonWC basic color='green' name="Exelente" click={this.exelentHandlerClick}></ButtonWC>
                            <ButtonWC basic color='red' name="Grandioso" click={this.grandiosoHandlerClick}></ButtonWC>
                        </div>
                    </Card.Content>
                </Card>

                <Card>
                    <Card.Content>
                        <Card.Header>Text Input Uncontroller Component</Card.Header>
                    </Card.Content>
                    <Card.Content extra>
                        <Form onSubmit={this.handlerSubmitForm} success>
                            <Form.Field>
                                <label>First Name</label>
                                <input placeholder='First Name' ref="name"/>
                            </Form.Field>
                            <Message success header='Form Completed' content="You're all signed up for the newsletter" />
                            <Button type='submit'>Submit</Button>
                        </Form>
                    </Card.Content>
                    <Card.Content>
                        <List>
                    {this.state.names.map((name,i)=><List.Item key={i}>{name}</List.Item>)}
                        </List>
                    </Card.Content>
                </Card>
                <Card>
                    <Card.Content>
                        <Card.Header>Text Input Controller Component</Card.Header>
                    </Card.Content>
                    <Card.Content extra>
                        <Form success error onSubmit={this.handlerSubmitFormController}>
                          
                            <Form.Field >
                                <label>First Name</label>
                                <input required placeholder='First Name' name="apellido" value={this.state.fields.apellido} onChange={this.onInputChange}/>
                            </Form.Field>
                            <Form.Field>
                                <label>First Name</label>
                                <input required placeholder='Email' name="email" value={this.state.fields.email} onChange={this.onInputChange}/>
                            </Form.Field>
                            <Button type='submit'>Submit</Button>
                        </Form>
                    </Card.Content>
                    <Card.Content>
                        <List>
        {this.state.camps.map(({apellido,email},i)=><List.Item key={i}>{apellido}({email})</List.Item>)}
                        </List>
                    </Card.Content>
                </Card>
            </Container>

        );
    }

}