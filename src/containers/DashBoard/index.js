import React, {Component} from 'react'
import {Item, Container, Segment,Header} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
class Dashboard extends Component {

    render() {
        return (
            <Container>
                <Segment>
                    <Header as="h1">Ejemplos React Js</Header>
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Item.Header as='a'>Votes</Item.Header>
                                <Item.Meta>Description</Item.Meta>
                                <Item.Description>
                                    Aplicacion para votar por una imagen
                                </Item.Description>
                                <Item.Extra>
                                    <Link to="/todo">Ver Todo List</Link>
                                </Item.Extra>
                            </Item.Content>
                            <Item.Content>
                                <Item.Header as='a'>Timers</Item.Header>
                                <Item.Meta>Description</Item.Meta>
                                <Item.Description>
                                    Aplicacion para organizar tiempos
                                </Item.Description>
                                <Item.Extra>
                                    <Link to="/timer">Ver Timer</Link>
                                </Item.Extra>
                            </Item.Content>
                            <Item.Content>
                                <Item.Header as='a'>Forms</Item.Header>
                                <Item.Meta>Description</Item.Meta>
                                <Item.Description>
                                   Manejo de eventos y formularios
                                </Item.Description>
                                <Item.Extra>
                                    <Link to="/forms">Ver Forms</Link>
                                </Item.Extra>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Segment>
            </Container>
        );
    }

}

export default Dashboard;