import React, {Component} from 'react'
import {Grid} from 'semantic-ui-react'
import TimerList from '../../components/TimerList'
import ToggleTimerForm from '../../components/ToggleTimerForm'
import uuid from 'uuid'
class Ejemplo2 extends Component {

    state = {
        timers: [
            {
                title: 'Practice squat',
                project: 'Gym Chores',
                id: uuid.v4(),
                elapsed: 5456099,
                runningSince: Date.now()
            }, {
                title: 'Bake squash',
                project: 'Kitchen Chores',
                id: uuid.v4(),
                elapsed: 1273998,
                runningSince: null
            }
        ]
    };

    handleCreateFormSubmit = (timer) => {
        this.createTimer(timer);
    };
    createTimer = (timer) => {
        const t = timer;
        this.setState({
            timers: this
                .state
                .timers
                .concat(t)
        });
    };

    handleEditFormSubmit = (attrs) => {
        this.updateTimer(attrs);
    };

    updateTimer = (attrs) => {
        this.setState({
            timers: this
                .state
                .timers
                .map((timer) => {
                    if (timer.id === attrs.id) {
                        return Object.assign({}, timer, {
                            title: attrs.title,
                            project: attrs.project
                        });
                    } else {
                        return timer;
                    }
                })
        });
    };

    handleTrashClick = (timerId) => {
        this.deleteTimer(timerId);
    };

    deleteTimer = (timerId) => {
        this.setState({
            timers: this
                .state
                .timers
                .filter(t => t.id !== timerId)
        });
    };
    render() {
        return (
            <Grid>
                <Grid.Column>
                    <TimerList timers={this.state.timers} onFormSubmit={this.handleEditFormSubmit}
                    onTrashClick={this.handleTrashClick}
                    ></TimerList>
                    <ToggleTimerForm onFormSubmit={this.handleCreateFormSubmit}></ToggleTimerForm>
                </Grid.Column>
            </Grid>
        );
    }

}

export default Ejemplo2;